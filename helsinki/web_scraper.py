import logging
import re
from os import path
from typing import Any, Dict, Tuple

import requests
from bs4 import BeautifulSoup

import config
import publisher


class WebScraper:
    """scrapes the c8s build page for the relevant packages and distil them.
    It also wraps them to a JSON message format and send across the same
    through rabbitmq publisher client
    TODO error handling and periodic poll decision(performance). Execution and
    message handling logic should be decoupled"""

    def __init__(
        self, file_with_packages: str, pub: publisher.Publisher
    ) -> None:
        self._url_dict = {}
        if file_with_packages:
            self._file_with_packages = file_with_packages
        else:
            self._file_with_packages = "packages.txt"

        self._package_names = open(
            path.join(
                path.dirname(path.realpath(__file__)), self._file_with_packages
            )
        ).readlines()
        if pub is None:
            self._publisher = publisher.Publisher()
        else:
            self._publisher = pub
        self._logger = logging.getLogger("Webscraper")

    def scrape(self, url: str) -> requests.Response:
        self._logger.info(f"Make a request to {url}")
        try:
            response_obj = requests.get(url)
        except ValueError:
            self._logger.error(
                f"Exception while calling \
            request.get, {ValueError}"
            )
        return response_obj

    def scrape_and_parse_html(self, url: str) -> Tuple[bool, str]:
        """scrap_and_parse_html parses the scrapped web result and looks for
        the builds of the relevant packages.If there is a build which is
        successful it extracts the required information to be sent.
        TODO include the logic of scrapping multi rows(builds)"""

        success = False
        page = self.scrape(url)

        soup = BeautifulSoup(page.content, "html.parser")
        result = soup.find("a", href=re.compile(config.scraper["re"]))

        if result:
            success = soup.find(
                "td",
                config.scraper["success_object"],
            )
        return success, result

    def execute(self) -> None:
        """execute checks the data scraped, validates and persists the data.
        It includes the logic to send only the changed data from upstream after
        the first run."""

        for package in self._package_names:
            url = config.scraper["url"] + package.strip()
            return_result = self.scrape_and_parse_html(url)

            if return_result[0]:
                if return_result[1]:
                    result = return_result[1].text
                    self._logger.debug(
                        f"Received package build information: \
                            {result}"
                    )

                    if url in self._url_dict:
                        if self._url_dict[url] == result:
                            self._logger.debug(
                                "Updated from c8s build from %r to %s"
                                % (self._url_dict[url], result)
                            )
                            continue
                    self._url_dict.update({url: result})
                    self.create_message(package.strip(), result)
                else:
                    self._url_dict.update({url: None})
                    self._logger.warning(f"No build found for {package}")
            else:
                self._logger.warning(
                    f"Build was unsuccessful for package \
                     {package}"
                )

    def create_message(self, package: str, nvr: str) -> None:
        """create_message creates the json message with the package information.
        TODO extend to follow a message schema. Add other paramters needed for
        a secure communication in the header probably a different module."""

        json_message = {
            "header": {
                "x-received-from": [
                    {
                        "uri": config.message["header"]["uri"],
                        "exchange": config.exchange["name"],
                    }
                ]
            },
            "message_id": config.message["message_id"],
            "content_encoding": config.message["content_encoding"],
            "body": {
                "artifact": {
                    "component": package,
                    "scratch": config.message["body"]["artifact"]["scratch"],
                    "id": config.message["body"]["artifact"]["id"],
                    "nvr": nvr,
                    "type": config.message["body"]["artifact"]["type"],
                    "issuer": config.message["body"]["artifact"]["issuer"],
                }
            },
            "routing_keys": config.routing_keys["c8s_build"],
        }
        self.send_data(json_message)

    def send_data(self, message: Dict[str, Any]) -> None:
        """Calls the publisher to handle the data to send."""

        self._publisher.publish(message)
