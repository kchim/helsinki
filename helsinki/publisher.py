import json
import logging
from typing import Any, Dict

import pika

import config


class Publisher:
    """Configures and establishes a connection on the channel with the exchange.
    Publish messages on the exchange"""

    def __init__(
        self, message_broker="amqp://guest:guest@rabbit-mq:5672/"
    ) -> None:
        self._connection = pika.BlockingConnection(
            pika.ConnectionParameters(host=message_broker)
        )
        self._channel = self._connection.channel()
        self._logger = logging.getLogger("Publisher")

    def publish(self, data: Dict[str, Any]) -> None:
        """Configure the message (refer the config) and send the message to exchange.
        TODO 1. Connection can be made asyncronous(performance & scale)
             2. Client should not die/crash if the server(message broker)
                is down(error handling)"""

        self._channel.exchange_declare(
            config.exchange["name"],
            config.exchange["exchange_type"],
            config.exchange["durable"],
        )

        try:
            self._channel.basic_publish(
                config.exchange["name"],
                data["routing_keys"],
                properties=pika.BasicProperties(
                    headers=data["header"],
                    message_id=data["message_id"],
                    content_encoding=data["content_encoding"],
                    app_id=config.publisher["app_id"],
                    content_type="application/json",
                ),
                body=json.dumps(data["body"]),
            )
            self._logger.debug(
                "Sent message == %r: \t   routing_key == %r \n\n"
                % (data["body"], data["routing_keys"])
            )
        except ValueError:
            self._logger.error(
                f"There was an exception: {ValueError}, \
                please restart!"
            )
            self._connection.close()
