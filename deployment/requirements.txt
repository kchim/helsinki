ansible==4.5.0
hvac==0.11.0
openshift==0.12.1
PyYAML==5.4.1
python-gitlab==2.10.0
